PERSONAL DATA PROTECTION NOTICE 


Purpose of Notice

This Privacy Notice is issued pursuant to the requirements under the Personal Data Protection Act 2010 (“Act”) and sets out how Trillion Cove Holdings Bhd (202001029951 (1386271-T)) and its subsidiary corporations, Trillion Cove Capital Sdn Bhd (Company No.: 202001012014 (1368334-M)) (“Trillion Cove”, “we”, “our”, “us”) collect, use, maintain, disclose and handle personal data of individuals in accordance with the Act. 
 
This Privacy Notice explains the type of personal data that Trillion Cove collects, the manner of the personal data is used, to whom and when will the personal data be disclosed to.

By providing your personal data to us, including any additional formation which you may subsequently provide, you are consenting to this Privacy Notice and the collection, use, access, storage, transfer and processing of your personal data as described in this Privacy Notice.


Personal Data 

The types of personal information that we collect from you (“Personal Data”) includes the following:

Personally Identifiable information e.g. Name, photo, gender, date of birth, nationality, passport/identification card number, passport expiry date, passport issuing country and country of residence;
Contact information e.g. address, email address and phone numbers;
Payment information e.g. credit or debit card information, including the name of cardholder, card number, card issuing bank, card issuing country, card expiry date and banking account details;
Technical information e.g. IP address


Purpose

The Personal Data is collected and processed for purposes which may include the following (“Purpose”):

For administrative purpose that is essential in the provision of our products and services to you including processing your application, request, registration for or subscription of the application created by us or our affiliates, services and/or use of our website/mobile applications;
For communication purposes including sending notification to you via email regarding change in terms and conditions of our products and/or services, responding to your enquiries or complaints; 
For carrying out our obligations and enforcing our rights arising from any contracts entered between you and us, including for billing and processing the payment transactions;
For statistical and marketing analysis. Information system management, system testing, maintenance and development of our products and/or services;
For marketing purposes including distribution of our newsletter or other promotional materials in respect of our products/and or services;
For credit checking purposes;
Such other purposes required by the relevant law and regulations.

Where you have indicated your consent to receiving marketing or promotional updates from us, you may opt-out from receiving such marketing or promotional material at any time. You may select the relevant “unsubscribe” option as may be provided in our marketing or promotional material or you may contact us at the details provided in Clause 15 below.




How and When We Collect Your Personal Data

We may collect the Personal Data from you in the following manner:

In your course of communication with us (for example, when you email us for queries or    submit your application or entry into a contest or promotional events sponsored by us);
Register yourself as a registered user on our website or our mobile applications;
Register your interest or when you request for information (including through our online portal and other available channels) or when you respond to any of our marketing materials;
Commence a business relationship with us; and
Visit or browse our website(s)/applications.

Other than the above, we may also obtain your personal data from third parties and such other sources where you have given your consent to the disclosure of information relating to you (for example, from your company) and where it is lawfully permitted.


Disclosure

In the course of providing you with our services and/or products, the management, administration and operations of the same may require us to disclose your Personal Data to the following third parties:-

our subsidiaries and affiliates;
the individuals, companies and organisations that act as our vendors, contractors, service providers and/or professional advisers;
the other third parties who are able to demonstrate that you have explicitly consented to the disclosure of your Personal Data by us to such third parties;
the enforcement, regulatory and governmental agencies whenever required by law;
credit reporting agencies. 

Your Personal Data may be disclosed to those business partners, authorised or contracted third parties, service providers or advertisers to make available promotions, offers, products or services, which may or may not belong to us but relate to the products or services you have elected to obtain from us.

However to ensure that you will not receive unwanted communications, only information with regards to promotions, offers, products or services which are relevant to your transaction with us, or that you have selected or marked as interested in your member profile, will be shared with the respective business partners, service providers or third parties.

In the circumstances set out in this Privacy Notice where we share your Personal Data to a third party, we will ensure that the security measures that such party has in place in relation to the processing of your data are at least as stringent as those employed by us if not better. This does not apply where we are required by law to pass your information to a third party.

Your Personal Data may also be disclosed or transferred to any of our actual and potential assignee, transferee or acquirer (within or outside Malaysia) (including our affiliates and subsidiaries) or our business, assets or group companies, or in connection with any corporate restructuring or exercise including the restructuring to transfer our business, assets and/or liabilities.


Access to and Correction of Your Personal Data
	
You may access your Personal Data by submitting your request in writing or via email to the contact details provided below.

If there are any changes to your Personal Data or if you wish to make a request to access or correct your Personal Data to ensure that it is accurate, complete and up-to-date, please contact us at the contact details provided below so that we may take steps to update your Personal Data.


Contact Information 
	
If you have any queries relating to this Privacy Notice, please contact our Customer Service at 03-86029229 during office hours (between 9.00 am to 6.00pm, Mondays to Fridays).


Updates to our Privacy Statement
	
We may amend this Notice from time to time and the updated version shall apply and supersede any and all previous versions. Please check our websites or mobile application(s) for information on our most up-to-date practices.


Protection Of Personal Data 

To safeguard your personal data from unauthorised access, collection, use, disclosure, copying, modification, disposal or similar risks, we have introduced appropriate administrative, physical and technical measures such as up-to-date antivirus protection, encryption and the use of privacy filters to secure all storage and transmission of personal data by us, and disclosing personal data both internally and to our authorised third party service providers and agents only on a need-to-know basis. 

You should be aware, however, that no method of transmission over the Internet or method of electronic storage is completely secure. While security cannot be guaranteed, we strive to protect the security of your information and are constantly reviewing and enhancing our information security measures. 


Retention Of Personal Data 

19.	We may retain your personal data for as long as it is necessary to fulfil the purpose for which it was collected, or as required or permitted by applicable laws. 

20. 	We will cease to retain your personal data, or remove the means by which the 
data can be associated with you, as soon as it is reasonable to assume that such retention no longer serves the purpose for which the personal data was collected, and is no longer necessary for legal or business purposes.


Consent 
	
You may submit a request to withdraw your consent at any time by contacting us through the contact set out in Clause 15. 

If you have provided us Personal Data of third party individuals, you are required to obtain the individual's prior consent and you represent and warrant that you had or have their consent or are otherwise entitled to provide their Personal Data to us. By providing us Personal Data of third party individual(s), you also warrant that the individual(s) is informed of and consents to the terms of this Privacy Notice.

In most instances, it is obligatory for you to provide us with your Personal Data in order to allow us to satisfy your request or provide you with the service that you have requested for.

However, we will provide you with an avenue to opt-out or unsubscribe from receiving marketing, communications, promotional offers, newsletters or any other communications from Trillion Cove.
