import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class WebViewViewModel extends ChangeNotifier {
  double _progress = 0;

  double get progress => _progress;

  setLoadProgress(int progress) {
    _progress = progress / 100;

    notifyListeners();
  }
}
