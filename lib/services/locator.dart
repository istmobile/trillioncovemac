
import 'package:stacked/stacked.dart';

import 'navigator_service.dart';

final locator = StackedLocator.instance;

void setupLocator() {
  /* services that will be accessible throughout the app,
  there's only ever one instance of it while the app is running.
  These services will be accessed by calling locator<ServiceName>()
   */
  locator.registerLazySingleton(() => NavigationService());
}
