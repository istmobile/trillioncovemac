import 'package:flutter/material.dart';

// insert hex code after 0xff
const Color primaryColor = Color(0xffffaa00);
const Color blackColor = Color(0xff2a2a2a);
const Color textColor = Color(0xff8f8f8f);
const Color hintTextColor = Color(0xff8f8f8f);
const Color bottomTextColor = Color(0xff4b4b4b);
const Color bottomContainerColor = Color(0xfff2f2f2);
const Color selectedColor = Color(0xff3d3d3d);
const Color unselectedColor = Color(0xffe8e8e8);
const Color progressUnselectedColor = Color(0xfff8f3e6);
const Color borderColor = Color(0xffe9e9e9);
const Color seasonsGrocerColor = Color(0xff54ab34);
const Color goMartColor = Color(0xffe1251b);
const Color k8Color = Color(0xff003e92);
const Color profileColor = Color(0xff464646);
const Color comingSoonColor = Color(0xffffbf00);
const Color redColor = Color(0xffbb0000);
const Color containerRedColor = Color(0xffe50019);
const Color descriptionTextColor = Color(0xff636363);
const Color greyColor = Color(0xff9f9f9f);
const Color homeContainerColor = Color(0xffE4E4E4);
const Color rayaYellowColor = Color(0xffffff05);
const Color rayaGreenColor = Color(0xff005300);
const Color spinRedColor = Color(0xffFF005F);
const Color spinYellowColor = Color(0xffFFFF00);
const Color spinGoldColor = Color(0xffB08146);
const Color spinPurpleColor = Color(0xffff0fff);
const Color spinCongratulationColor = Color(0xfffbf23b);

MaterialColor themPrimaryColor = MaterialColor(0xffe30051, color);

Map<int, Color> color = {
  50: Color.fromRGBO(4, 131, 184, .1),
  100: Color.fromRGBO(4, 131, 184, .2),
  200: Color.fromRGBO(4, 131, 184, .3),
  300: Color.fromRGBO(4, 131, 184, .4),
  400: Color.fromRGBO(4, 131, 184, .5),
  500: Color.fromRGBO(4, 131, 184, .6),
  600: Color.fromRGBO(4, 131, 184, .7),
  700: Color.fromRGBO(4, 131, 184, .8),
  800: Color.fromRGBO(4, 131, 184, .9),
  900: Color.fromRGBO(4, 131, 184, 1),
};

extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}