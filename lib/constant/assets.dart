import 'package:flutter/cupertino.dart';

class Assets {
  static final String iconSplash = "assets/images/splash_icon.png";
  static final String mainLogo = "assets/images/main_logo.png";
  static final String mainBg = "assets/images/main_bg.png";
  static final String welcomeBanner = "assets/images/welcome_banner.png";
  static final String backArrow = "assets/images/back_arrow.png";
  static final String rightArrow = "assets/images/icon-arrow-right.png";
  static final String menuIcon = "assets/images/menu_icon.png";
  static final String closeIcon = "assets/images/close_icon.png";

  static final String phoneIcon = "assets/images/phone_icon.png";
  static final String emailIcon = "assets/images/email_icon.png";
  static final String webIcon = "assets/images/web_icon.png";

  static final String page1 = "assets/images/page1.png";
  static final String page2 = "assets/images/page2.png";
  static final String page3 = "assets/images/page3.png";


  static final bool productionAppFlag = true;

  static final String email =  productionAppFlag? live_email : uat_email;
  static final String learnMore = productionAppFlag? live_learnMore : uat_learnMore;
  static final String landingPortal = productionAppFlag? live_landingPortal : uat_landingPortal;
  static final String landingPortal1 = productionAppFlag? live_landingPortal1 : uat_landingPortal1;
  static final String logout = productionAppFlag? live_logout : uat_logout;
  static final String logout1 = productionAppFlag? live_logout1 : uat_logout1;
  static final String contactUs = productionAppFlag? live_contactUs : uat_contactUs;
  static final String onCompleteEKYC = productionAppFlag? live_onCompleteEKYC : uat_onCompleteEKYC;
  static final String onGoingEKYC = productionAppFlag? live_onGoingEKYC : uat_onGoingEKYC;

  /*static final String email =  uat_email;
  static final String learnMore = uat_learnMore;
  static final String landingPortal = uat_landingPortal;
  static final String landingPortal1 = uat_landingPortal1;
  static final String logout = uat_logout;
  static final String logout1 = uat_logout1;
  static final String contactUs = uat_contactUs;
  static final String onCompleteEKYC = uat_onCompleteEKYC;
  static final String onGoingEKYC = uat_onGoingEKYC;*/

  //url live


  //https://lendingportal.trillioncovecapital.com.my/JACCESS/m/

  static final String live_email =  "info@trillioncovecapital.com.my";
  static final String live_learnMore = "https://trillioncovecapital.com.my/";
  static final String live_landingPortal = "https://lendingportal.trillioncovecapital.com.my/JACCESS/m/";
  static final String live_landingPortal1 = "https://lendingportal.trillioncovecapital.com.my/JACCESS/m/?reset=1";
  static final String live_logout = "https://lendingportal.trillioncovecapital.com.my/JACCESS/m/cust/logout.php?reset=1";
  static final String live_logout1 = "https://lendingportal.trillioncovecapital.com.my/JACCESS/m/cust/logout.php";
  static final String live_contactUs = "https://trillioncovecapital.com.my";
  static final String live_onCompleteEKYC = "https://lendingportal.trillioncovecapital.com.my/JACCESS/m/cust/ekycComplete.php";
  static final String live_onGoingEKYC = "https://eonboardingsme.ctos.com.my/gateway";


  // url uat

  static final String uat_email = "https://trillioncovecapital.com.my/";
  static final String uat_learnMore = "https://trillioncovecapital.com.my/";
  static final String uat_landingPortal = "https://uat.lendingportal.upay2us.net/JACCESS/m/";
  static final String uat_landingPortal1 = "https://uat.lendingportal.upay2us.net/JACCESS/m/?reset=1";
  static final String uat_logout = "https://uat.lendingportal.upay2us.net/JACCESS/m/cust/logout.php?reset=1";
  static final String uat_logout1 = "https://uat.lendingportal.upay2us.net/JACCESS/m/cust/logout.php";
  static final String uat_contactUs = "https://trillioncovecapital.com.my";
  static final String uat_onCompleteEKYC = "https://uat.lendingportal.upay2us.net/JACCESS/m/cust/ekycComplete.php";
  static final String uat_onGoingEKYC = "https://uat-eonboarding.ctos.com.my";


  // url Dev

  static final String dev_email = "https://trillioncovecapital.com.my/";
  static final String dev_learnMore = "https://trillioncovecapital.com.my/";
  static final String dev_landingPortal = "https://cloud.juristechnologies.com/JACCESS_OT/m/";
  static final String dev_landingPortal1 = "https://cloud.juristechnologies.com/JACCESS_OT/m/?reset=1";
  static final String dev_logout = "https://cloud.juristechnologies.com/JACCESS_OT/m/cust/logout.php?reset=1";
  static final String dev_logout1 = "https://cloud.juristechnologies.com/JACCESS_OT/m/cust/logout.php";
  static final String dev_contactUs = "https://trillioncovecapital.com.my";
  static final String dev_onCompleteEKYC = "https://cloud.juristechnologies.com/JACCESS_OT/m/cust/ekycComplete.php";
  static final String dev_onGoingEKYC = "https://uat-eonboarding.ctos.com.my";



  static PageController page_Controller = PageController(initialPage: 0);


}
