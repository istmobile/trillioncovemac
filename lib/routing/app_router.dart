
import 'package:cove/routing/route_argument.dart';
import 'package:cove/routing/route_names.dart';
import 'package:cove/view/contactus_view.dart';
import 'package:cove/view/home_view.dart';
import 'package:cove/view/learnmore_view.dart';
import 'package:cove/view/page_one.dart';
import 'package:cove/view/page_one_temp.dart';
import 'package:cove/view/page_three.dart';
import 'package:cove/view/page_two.dart';
import 'package:cove/view/pdf_view.dart';
import 'package:cove/view/privacy_view.dart';
import 'package:cove/view/splash_view.dart';
import 'package:cove/view/welcome_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class AppRouter {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case SplashRoute:
        return MaterialPageRoute(builder: (context) => SplashView());
      case HomeRoute:
        return MaterialPageRoute(builder: (context) => HomeView());
      case WelcomeRoute:
        return MaterialPageRoute(builder: (context) => WelcomeView());
      case PrivacyRoute:
        return MaterialPageRoute(builder: (context) => PrivacyView());
      case ContactUsRoute:
        return MaterialPageRoute(builder: (context) => ContactUsView());
      case LearnMoreRoute:
        return MaterialPageRoute(builder: (context) => LearnMoreView());
      case PageOneTempRoute:
        return MaterialPageRoute(builder: (context) => PageOneTempView());
      case PageOneRoute:
        return MaterialPageRoute(builder: (context) => PageOneView());
      case PageTwoRoute:
        return MaterialPageRoute(builder: (context) => PageTwoView());
      case PageThreeRoute:
        return MaterialPageRoute(builder: (context) => PageThreeView());
      case PdfRoute:
      final PdfArguments args = settings.arguments;
        return MaterialPageRoute(builder: (context) => PdfView(url: args.url));
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No route defined yet'),
                  ),
                ));
    }
  }
}

PageRoute _getPageRoute(Widget child, RouteSettings settings) {
  return _FadeRoute(child: child, routeName: settings.name);
}

class _FadeRoute extends PageRouteBuilder {
  final Widget child;
  final String routeName;

  _FadeRoute({this.child, this.routeName})
      : super(
          settings: RouteSettings(name: routeName),
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              child,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) =>
              FadeTransition(
            opacity: animation,
            child: child,
          ),
        );
}
