

import 'package:flutter/cupertino.dart';

class PdfArguments {
  final String url;

  PdfArguments(
      {@required this.url});
}