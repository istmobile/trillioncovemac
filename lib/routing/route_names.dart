
const String SplashRoute = 'splash';
const String HomeRoute = 'home';
const String WelcomeRoute = 'welcome';
const String PrivacyRoute = 'privacy';
const String ContactUsRoute = 'contact-us';
const String LearnMoreRoute = 'learn-more';
const String PdfRoute = 'pdf-route';
const String PageOneTempRoute = 'page-one-temp';
const String PageOneRoute = 'page-one';
const String PageTwoRoute = 'page-two';
const String PageThreeRoute = 'page-three';
