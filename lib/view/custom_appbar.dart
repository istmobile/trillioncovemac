import 'package:cove/constant/assets.dart';
import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget {
  const CustomAppBar(
      {@required this.text,
      @required this.backgroundColor,
      @required this.backIconBackgroundColor,
      @required this.iconColor});

  final Color iconColor;
  final Color backgroundColor;
  final Color backIconBackgroundColor;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.grey[200],
      ),
      child: Padding(
        padding: EdgeInsets.only(top: 35),
        child: Row(
          children: [
            Expanded(
              flex: 0,
              child: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  // decoration: BoxDecoration(
                  //   color: backIconBackgroundColor,
                  //   borderRadius: BorderRadius.only(
                  //     topRight: Radius.circular(10),
                  //     bottomRight: Radius.circular(10),
                  //   ),
                  // ),
                  margin: EdgeInsets.only(right: 20),
                  height: 56,
                  width: 65,
                  padding: EdgeInsets.all(15),
                  child: Center(
                    child: Image(
                      image: AssetImage(Assets.backArrow),
                      width: 50,
                      height: 50,
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 20),
                height: 56,
                width: 65,
                // decoration: BoxDecoration(
                //   color: backgroundColor,
                //   borderRadius: BorderRadius.only(
                //     topLeft: Radius.circular(10),
                //     bottomLeft: Radius.circular(10),
                //   ),
                // ),
                child: Text(
                  '$text',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Quicksand-Bold',
                      color: Colors.purple),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
