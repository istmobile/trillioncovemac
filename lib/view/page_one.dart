import 'package:cove/constant/app_colors.dart';
import 'package:cove/constant/assets.dart';
import 'package:cove/customView/circle_painter.dart';
import 'package:cove/customView/line_painter.dart';
import 'package:cove/routing/route_names.dart';
import 'package:cove/services/locator.dart';
import 'package:cove/services/navigator_service.dart';
import 'package:cove/versionCheck/version_check.dart';
import 'package:cove/view/page_one_temp.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:stacked/stacked.dart';

class PageOneView extends StatelessWidget {
  final NavigationService _navigationService = locator<NavigationService>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    //SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);


    //versionCheck(context);

    return Scaffold(
      key: _scaffoldKey,
      body: _getBodyView(context),
    );
  }

  _getBodyView(BuildContext context) {
    return Center(
      child: Stack(
        alignment: AlignmentDirectional.bottomCenter,
        overflow: Overflow.visible,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                  Assets.mainBg,
                ),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Positioned(
            child: Container(
              margin: EdgeInsets.all(10.0),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Image(
                      image: AssetImage(Assets.page1),
                      width: MediaQuery.of(context).size.width,
                      height: 300,
                    ),
                  ),
                  Text('''Quick and easy \n financing solutions''',
                    style: TextStyle(
                        color: Colors.purple[800],
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Quicksand-Bold',
                        fontSize: 20),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  SizedBox(
                    width: 300,
                    child: Text(
                      "Explore our wide range of offerings that cater to both personal and business needs.",
                      style: TextStyle(
                          color: Colors.grey[800],
                          fontWeight: FontWeight.normal,
                          fontFamily: 'Quicksand-Regular',
                          fontSize: 15),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Row(children: <Widget>[
                    Container(
                      color: Colors.white,
                      width: 15,
                      height: 5,
                      child: CustomPaint(
                        foregroundPainter: LinePainter(),
                      ),
                    ),
                    Container(
                      color: Colors.white,
                      width: 2,
                      height: 2,
                    ),
                    Container(
                      color: Colors.white,
                      width: 5,
                      height: 5,
                      child: CustomPaint(
                        painter: CirclePainter(),
                      ),
                    ),
                    Container(
                      color: Colors.white,
                      width: 2,
                      height: 2,
                    ),
                    Container(
                      color: Colors.white,
                      width: 5,
                      height: 5,
                      child: CustomPaint(
                        painter: CirclePainter(),
                      ),
                    ),
                  ]),
                  SizedBox(
                    height: 30,
                  ),
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.orange[300]),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              side: BorderSide(
                                  color: Colors.orange[500], width: 2.0))),
                    ),
                    child: Row(
                      children: [
                        Text('Next',
                            style: TextStyle(
                                color: Colors.grey[800],
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Quicksand-Regular',
                                fontSize: 18)),
                        Image(
                          image: AssetImage(Assets.rightArrow),
                          width: 35,
                          height: 15,
                        ),
                      ],
                    ),
                    onPressed: () {
                      Assets.page_Controller.animateToPage(1, duration: Duration(microseconds: 100), curve: Curves.bounceIn);

                    },
                  ),
                ],
              ),
            ),
            top: 10,
          ),
        ],
      ),
    );
  }
}
