import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class PdfView extends StatefulWidget {
  final String url;

  const PdfView({
    Key key,
    this.url,
  }) : super(key: key);

  @override
  MyPdfState createState() => MyPdfState(this.url);
}

class MyPdfState extends State<PdfView> {
  bool _isLoading = true;

  //PDFDocument document;
  String stringUrl;
  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();

  MyPdfState(this.stringUrl);

  @override
  void initState() {
    super.initState();
    _pdfViewerKey.currentState?.openBookmarkView();
    print("pdf file" + stringUrl);
    loadDocument();
  }

  loadDocument() async {
    print("pdf file" + stringUrl);
    //document = await PDFDocument.fromURL(stringUrl);

    setState(() => _isLoading = false);
  }

  /*changePDF(value) async {
    setState(() => _isLoading = true);
    document = await PDFDocument.fromURL(stringUrl);

    setState(() => _isLoading = false);
  }*/

  @override
  Widget build(BuildContext context) {
    //SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: SystemUiOverlay.values);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        /*appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),*/
        floatingActionButton: SizedBox(
          height: 40,
          width: 40,
          child: FloatingActionButton(
            child: new Icon(
              Icons.arrow_back,
              color: Colors.purple,
            ),
            backgroundColor: Colors.white,
            focusColor: Colors.white,
            disabledElevation: 0.0,
            hoverColor: Colors.white,
            foregroundColor: Colors.white,
            hoverElevation: 0.0,
            shape: CircleBorder(side: BorderSide.none),
            onPressed: () {
              Navigator.pop(context);
              ///Navigator.pushReplacementNamed(context, "/logout");
            },
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
        backgroundColor: Colors.white,
        body: Center(
          child: _isLoading
              ? Center(child: CircularProgressIndicator())
              : SfPdfViewer.network(
                  stringUrl,
                  key: _pdfViewerKey,
                ),
        ),
      ),
    );
  }
}
