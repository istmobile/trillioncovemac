import 'package:cove/constant/app_colors.dart';
import 'package:cove/constant/assets.dart';
import 'package:cove/routing/route_names.dart';
import 'package:cove/services/locator.dart';
import 'package:cove/services/navigator_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:stacked/stacked.dart';
import 'package:url_launcher/url_launcher.dart';

class WelcomeView extends StatelessWidget {
  final NavigationService _navigationService = locator<NavigationService>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    //SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);

    return Scaffold(
      appBar:  AppBar (
        toolbarHeight: 1,
      ),
      key: _scaffoldKey,
      endDrawer: Drawer(
          child: Container(
        decoration: new BoxDecoration(
          color: HexColor.fromHex('#3C1C43'),
        ),
        child: ListView(children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Align(
              alignment: Alignment.centerRight,
              child: InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Image(
                  image: AssetImage(Assets.closeIcon),
                  width: 50,
                  height: 50,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: ListTile(
                title: Text(
                  'PRIVACY NOTICE',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontFamily: 'Quicksand-Bold',
                      fontSize: 18),
                  textAlign: TextAlign.left,
                ),
                onTap: () {
                  _navigationService.navigateTo(PrivacyRoute);
                },
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
            child: Divider(color: Colors.white),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: ListTile(
                title: Text(
                  'CONTACT US',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontFamily: 'Quicksand-Bold',
                      fontSize: 18),
                  textAlign: TextAlign.left,
                ),
                onTap: () {
                  _navigationService.navigateTo(ContactUsRoute);
                },
              ),
            ),
          ),
        ]),
      )),
      body: _getBodyView(context),
    );
  }

  _getBodyView(BuildContext context) {
    return Center(
      child: Stack(
        alignment: AlignmentDirectional.bottomCenter,
        overflow: Overflow.visible,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                  Assets.mainBg,
                ),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Positioned(
            child: Container(
              margin: EdgeInsets.all(35.0),
              child: Image(
                image: AssetImage(Assets.mainLogo),
                width: 80,
                height: 50,
              ),
            ),
            left: 0,
            top: 0,
          ),
          Positioned(
            child: Container(
              margin: EdgeInsets.all(35.0),
              child: InkWell(
                onTap: () {
                  _scaffoldKey.currentState.openEndDrawer();
                },
                child: Image(
                  image: AssetImage(Assets.menuIcon),
                  width: 50,
                  height: 50,
                ),
              ),
            ),
            right: 0,
            top: 0,
          ),
          Positioned(
            child: Container(
              margin: EdgeInsets.only(bottom: 10),
              child: Column(
                children: [
                  Text(
                    "Welcome",
                    style: TextStyle(
                        color: Colors.purple[800],
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Quicksand-Bold',
                        fontSize: 22),
                    textAlign: TextAlign.left,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(25.0, 0.0, 25.0, 0.0),
                    child: Image(
                      image: AssetImage(Assets.welcomeBanner),
                      width: MediaQuery.of(context).size.width,
                      height: 150,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    "Quick and easy financing solutions",
                    style: TextStyle(
                        color: Colors.purple[800],
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Quicksand-Bold',
                        fontSize: 20),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  SizedBox(
                    width: 300,
                    child: Text(
                      "Easily apply for fast and convenient financing options that can be approved within 48 hours for personal or business needs. Discover the benefits of applying for financial assistance with Trillion Cove Capital.",
                      style: TextStyle(
                          color: Colors.grey[800],
                          fontWeight: FontWeight.normal,
                          fontFamily: 'Quicksand-Regular',
                          fontSize: 15),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  InkWell(
                    child: Text(
                      "Learn More",
                      style: TextStyle(
                          decoration: TextDecoration.underline,
                          color: Colors.purple[800],
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Quicksand-Bold',
                          fontSize: 18),
                      textAlign: TextAlign.left,
                    ),
                    onTap: () {
                      _navigationService.navigateTo(
                        LearnMoreRoute,
                      );
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.orange[300]),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              side: BorderSide(
                                  color: Colors.orange[500], width: 2.0))),
                    ),
                    child: Row(
                      children: [
                        Text('Apply Now',
                            style: TextStyle(
                                color: Colors.grey[800],
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Quicksand-Regular',
                                fontSize: 18)),
                        Image(
                          image: AssetImage(Assets.rightArrow),
                          width: 40,
                          height: 20,
                        ),
                      ],
                    ),
                    onPressed: () {
                      _navigationService.navigateTo(
                        HomeRoute,
                      );
                    },
                  ),
                ],
              ),
            ),
            top: 50,
          ),
          /*Positioned(
            child: Container(
              margin: EdgeInsets.only(bottom: 5),
              child: Column(children: [
                ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all<Color>(Colors.orange[300]),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(2.0),
                            side: BorderSide(
                                color: Colors.orange[500], width: 1.0))),
                  ),
                  child: Row(
                    children: [
                      Text('Go to Browser',
                          style: TextStyle(
                              color: Colors.grey[800],
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Quicksand-Regular',
                              fontSize: 8)),
                    ],
                  ),
                  onPressed: () {
                    _launchURL();
                  },
                ),
              ],
              ),
            ),
            bottom: 2,
          ),*/
        ],
      ),
    );
  }
}

_launchURL() async {
  if (await canLaunch(Assets.landingPortal)) {
    await launch(Assets.landingPortal);
  } else {
    throw 'Could not launch landingPortal';
  }
}
