import 'package:cove/constant/assets.dart';
import 'package:cove/routing/route_argument.dart';
import 'package:cove/routing/route_names.dart';
import 'package:cove/services/locator.dart';
import 'package:cove/services/navigator_service.dart';
import 'package:cove/view_model/webview_viewmodel.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:provider/provider.dart';
import 'package:stacked/stacked.dart';

import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:isolate';
import 'dart:typed_data';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:path_provider/path_provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../main.dart';
import 'custom_appbar.dart';

// ignore: must_be_immutable
//class HomeView extends StatelessWidget {
class HomeView extends StatefulWidget {
  NewView createState() => NewView();

  final NavigationService _navigationService = locator<NavigationService>();

/*void _launchURL(String _url) async => await canLaunch(_url)
      ? await launch(_url)
      : throw 'Could not launch $_url';*/
}

class NewView extends State<HomeView> {
  final NavigationService _navigationService = locator<NavigationService>();

  /*InAppWebViewController _webViewController;
  InAppWebViewController _webViewPopupController;*/

  bool downloading = false;

  //////////// ROHIT
  final GlobalKey webViewKey = GlobalKey();

  InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
      crossPlatform: InAppWebViewOptions(
        useShouldOverrideUrlLoading: true,
        mediaPlaybackRequiresUserGesture: false,
        allowUniversalAccessFromFileURLs: true,
        javaScriptCanOpenWindowsAutomatically: true,
        allowFileAccessFromFileURLs: true,
        useOnDownloadStart: true,
        javaScriptEnabled: true,
        clearCache: false,
        cacheEnabled: false,
        verticalScrollBarEnabled: true,
        supportZoom: true,

        //userAgent: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36",
        userAgent: "Chrome/78.0.3904.108 Mozilla/5.0 (iPhone; CPU iPhone OS 14_6 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.1 Mobile/15E148 Safari/604.1",


      ),
      android: AndroidInAppWebViewOptions(
        useHybridComposition: true,
        allowFileAccess: true,
      ),
      ios: IOSInAppWebViewOptions(
        allowsInlineMediaPlayback: true,
      ));

  String url = "";
  final urlController = TextEditingController();

  @override
  void initState() {
    super.initState();

    progress = 0;

    // FlutterDownloader.registerCallback(downloadCallback);

    pullToRefreshController = PullToRefreshController(
      options: PullToRefreshOptions(
        color: Colors.blue,
      ),
      onRefresh: () async {
        if (Platform.isAndroid) {
          webViewController?.reload();
        } else if (Platform.isIOS) {
          webViewController?.loadUrl(
              urlRequest: URLRequest(url: await webViewController?.getUrl()));
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
   // SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: SystemUiOverlay.values);
    return ViewModelBuilder<WebViewViewModel>.nonReactive(
      viewModelBuilder: () => WebViewViewModel(),
      builder: (_context, model, child) => Scaffold(
        /*floatingActionButton: SizedBox(
          height: 40,
          width: 40,
          child: FloatingActionButton(
            child: new Icon(
              Icons.arrow_back,
              color: Colors.purple,
            ),
            backgroundColor: Colors.white,
            focusColor: Colors.white,
            disabledElevation: 0.0,
            hoverColor: Colors.white,
            foregroundColor: Colors.white,
            hoverElevation: 0.0,
            shape: CircleBorder(side: BorderSide.none),
            onPressed: () {
              print("web site"+urlController.text.toString());
              if (urlController.text.toString() == Assets.landingPortal
                ||  urlController.text.toString() == Assets.logout
                ||  urlController.text.toString() == Assets.logout1) {
                Navigator.pop(context);
              } else {
                webViewController?.goBack();
              }

              ///Navigator.pushReplacementNamed(context, "/logout");
            },
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
        backgroundColor: Colors.white,*/
        appBar:  AppBar (
          toolbarHeight: 1,
          backgroundColor: Colors.grey[200],
        ),
        body: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.grey[200],
              ),
              child: Padding(
                padding: EdgeInsets.only(top: 15),
                child: Row(
                  children: [
                    Expanded(
                      flex: 0,
                      child: GestureDetector(
                        onTap: () {
                          if (urlController.text.toString() == Assets.landingPortal
                              ||  urlController.text.toString() == Assets.landingPortal1
                              ||  urlController.text.toString() == Assets.logout
                              ||  urlController.text.toString() == Assets.logout1) {
                            Navigator.pop(context);
                          } else if(urlController.text.toString().contains(Assets.onCompleteEKYC) || urlController.text.toString().contains(Assets.onGoingEKYC)){

                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    content: Stack(
                                      overflow: Overflow.visible,
                                      children: <Widget>[
                                        Positioned(
                                          right: -40.0,
                                          top: -40.0,
                                          child: InkResponse(
                                            onTap: () {
                                              Navigator.of(context).pop();
                                            },
                                            child: CircleAvatar(
                                              child: Icon(Icons.close),
                                              backgroundColor: Colors.red,
                                            ),
                                          ),
                                        ),
                                        Form(
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Padding(
                                                padding: EdgeInsets.all(8.0),
                                                child: Text("Are you sure you want to go home page?"),
                                              ),
                                              Padding(
                                                padding:
                                                const EdgeInsets.all(8.0),
                                                child: RaisedButton(
                                                  child: Text("Yes"),
                                                  onPressed: () {
                                                    webViewController.loadUrl( urlRequest: URLRequest(url: Uri.parse(Assets.landingPortal)));
                                                    Navigator.of(context).pop();

                                                  },
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                });

                          }else {
                            webViewController?.goBack();
                          }
                        },
                        child: Container(

                          margin: EdgeInsets.only(right: 15),
                          height: 40,
                          width: 65,
                          child: Center(
                            child: Image(
                              image: AssetImage(Assets.backArrow),
                              width: 30,
                              height: 25,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            /*Padding(
              padding: const EdgeInsets.all(25.0),
              child: Text(""),
            ),*/
            /*
            ButtonBar(
              buttonHeight: 50,
              alignment: MainAxisAlignment.values.first,
              children: <Widget>[
                ElevatedButton(
                  child: Icon(Icons.arrow_back),
                  onPressed: () {

                    if(urlController.text.toString()==Assets.landingPortal){
                      Navigator.pop(context);
                    }else{
                      webViewController?.goBack();
                    }
                  },
                ),
              ],
            ),*/
            /*CustomAppBar(
              text: ' ',
              backgroundColor: Colors.grey[100],
              iconColor: Colors.purple,
              backIconBackgroundColor: Colors.white,
            ),
            Selector<WebViewViewModel, double>(
              selector: (context, model) => model.progress,
              builder: (context, progress, child) => Container(
                  margin: EdgeInsets.only(top: 5, right: 25, left: 25),
                  child: progress < 1.0
                      ? LinearProgressIndicator(
                    minHeight: 5,
                    backgroundColor: Colors.red,
                  )
                      : Container()),
            ),*/
            //////////// ROHIT

            Expanded(
              child: Stack(
                children: [
                  InAppWebView(
                    key: webViewKey,
                    onLoadHttpError: (controller, url, statusCode, description) {
                      Navigator.pop(context);
                    },

                    initialUrlRequest:
                        URLRequest(url: Uri.parse(Assets.landingPortal)),
                    initialOptions: options,
                    //pullToRefreshController: pullToRefreshController,
                    onWebViewCreated: (controller) {
                      webViewController = controller;
                    },
                    /*androidOnPermissionRequest: (InAppWebViewController controller, String origin, List<String> resources) async {
                      return PermissionRequestResponse(resources: resources, action: PermissionRequestResponseAction.GRANT);
                    },*/
                    androidOnPermissionRequest: (InAppWebViewController controller, String origin, List<String> resources) async {
                    print(urlController.text.toString());
                    print(resources);
                    return PermissionRequestResponse(resources: resources, action: PermissionRequestResponseAction.GRANT);
                  },
                    onLoadStart: (controller, url) {
                      this.url = url.toString();
                      urlController.text = this.url;
                      print(urlController.text.toString());

                      /*setState(() {
                        this.url = url.toString();
                        urlController.text = this.url;
                      });*/
                    },
                    /*androidOnPermissionRequest:
                        (controller, origin, resources) async {
                      return PermissionRequestResponse(
                          resources: resources,
                          action: PermissionRequestResponseAction.GRANT);
                    },*/
                    shouldOverrideUrlLoading:
                        (controller, navigationAction) async {
                      var uri = navigationAction.request.url;

                      if (![
                        "http",
                        "https",
                        "file",
                        "chrome",
                        "data",
                        "javascript",
                        "about"
                      ].contains(uri.scheme)) {
                        if (await canLaunch(url)) {
                          // Launch the App
                          await launch(
                            url,
                          );
                          // and cancel the request
                          return NavigationActionPolicy.CANCEL;
                        }
                      }

                      return NavigationActionPolicy.ALLOW;
                    },
                    onDownloadStart: (controller, url) async {
                      print("onDownloadStart $url storage name ");

                      _navigationService.navigateTo(PdfRoute,
                          arguments: PdfArguments(
                            url: url.toString(),
                          ));
                      //downloadFile(url.toString());
                      /*dio=Dio();
                    courseContent.add(Course(title:"Chapter 2",path:"https://www.cs.purdue.edu/homes/ayg/CS251/slides/chap2.pdf"));*/
                      /*final taskId = await FlutterDownloader.enqueue(
                      url: url.toString(),
                      savedDir: (await getExternalStorageDirectory()).path,
                      showNotification: true,
                      // show download progress in status bar (for Android)
                      openFileFromNotification:
                          true, // click on notification to open downloaded file (for Android)
                    );*/
                    },
                    onLoadStop: (controller, url) async {
                      //pullToRefreshController.endRefreshing();
                      this.url = url.toString();
                      urlController.text = this.url;
                      /*setState(() {
                        this.url = url.toString();
                        urlController.text = this.url;
                      });*/
                    },
                    onLoadError: (controller, url, code, message) {
                      //pullToRefreshController.endRefreshing();
                    },
                    onProgressChanged: (controller, progress) {
                      model.setLoadProgress(progress);
                      if (progress == 100) {
                        //pullToRefreshController.endRefreshing();
                      }
                      this.url = url.toString();
                      urlController.text = this.url;
                      /*setState(() {
                        this.url = url.toString();
                        urlController.text = this.url;
                      });*/
                    },
                    onUpdateVisitedHistory: (controller, url, androidIsReload) {
                      this.url = url.toString();
                      urlController.text = this.url;
                      /*setState(() {
                        this.url = url.toString();
                        urlController.text = this.url;
                      });*/
                    },
                    onConsoleMessage: (controller, consoleMessage) {
                      print(consoleMessage);
                    },
                  ),
                  /*progress < 1.0
                      ? LinearProgressIndicator(value: progress)
                      : Container(),*/
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

//// old Code
/*

/*InAppWebViewController _webViewController;
  InAppWebViewController _webViewPopupController;*/

  //////////// ROHIT
  final GlobalKey webViewKey = GlobalKey();

  InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
      crossPlatform: InAppWebViewOptions(
        useShouldOverrideUrlLoading: true,
        mediaPlaybackRequiresUserGesture: false,
        allowUniversalAccessFromFileURLs: true,
        javaScriptCanOpenWindowsAutomatically: true,
        allowFileAccessFromFileURLs: true,
        useOnDownloadStart: true,
      ),
      android: AndroidInAppWebViewOptions(
        useHybridComposition: true,
        allowFileAccess: true,
      ),
      ios: IOSInAppWebViewOptions(
        allowsInlineMediaPlayback: true,
      ));


  String url = "";
  final urlController = TextEditingController();


  @override
  void initState() {

    progress = 0;

   // FlutterDownloader.registerCallback(downloadCallback);

    pullToRefreshController = PullToRefreshController(
      options: PullToRefreshOptions(
        color: Colors.blue,
      ),
      onRefresh: () async {
        if (Platform.isAndroid) {
          webViewController?.reload();
        } else if (Platform.isIOS) {
          webViewController?.loadUrl(
              urlRequest: URLRequest(url: await webViewController?.getUrl()));
        }
      },
    );
  }


 static void downloadCallback(String id, DownloadTaskStatus status, int progress) {
    if (debug) {
      print(
          'Isolate Callback: task ($id) is in status ($status) and process ($progress)');
    }
    final SendPort send =
    IsolateNameServer.lookupPortByName('downloader_send_port');
    send.send([id, status, progress]);
  }


  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<WebViewViewModel>.nonReactive(
      viewModelBuilder: () => WebViewViewModel(),
      builder: (_context, model, child) => Scaffold(
        body: Column(
          children: [
            CustomAppBar(
              text: ' ',
              backgroundColor: Colors.grey[100],
              iconColor: Colors.purple,
              backIconBackgroundColor: Colors.white,
            ),
            Selector<WebViewViewModel, double>(
              selector: (context, model) => model.progress,
              builder: (context, progress, child) => Container(
                  margin: EdgeInsets.only(top: 5, right: 25, left: 25),
                  child: progress < 1.0
                      ? LinearProgressIndicator(
                          minHeight: 5,
                          backgroundColor: Colors.red,
                        )
                      : Container()),
            ),
            //////////// ROHIT
        Expanded(
          child: Stack(
            children: [ InAppWebView(
                  key: webViewKey,
                  initialUrlRequest: URLRequest(
                      url: Uri.parse(
                          "https://lendingportal.capital.trillioncove.com/JACCESS/m/")),
                  initialOptions: options,
                  pullToRefreshController: pullToRefreshController,
                  onWebViewCreated: (controller) {
                    webViewController = controller;
                  },
                  onLoadStart: (controller, url) {
                    this.url = url.toString();
                    urlController.text = this.url;
                    /*setState(() {
                        this.url = url.toString();
                        urlController.text = this.url;
                      });*/
                  },
                  androidOnPermissionRequest:
                      (controller, origin, resources) async {
                    return PermissionRequestResponse(
                        resources: resources,
                        action: PermissionRequestResponseAction.GRANT);
                  },
                  shouldOverrideUrlLoading:
                      (controller, navigationAction) async {
                    var uri = navigationAction.request.url;

                    if (![
                      "http",
                      "https",
                      "file",
                      "chrome",
                      "data",
                      "javascript",
                      "about"
                    ].contains(uri.scheme)) {
                      if (await canLaunch(url)) {
                        // Launch the App
                        await launch(
                          url,
                        );
                        // and cancel the request
                        return NavigationActionPolicy.CANCEL;
                      }
                    }

                    return NavigationActionPolicy.ALLOW;
                  },
                  onDownloadStart: (controller, url) async {
                    print("onDownloadStart $url storage name ");
                    downloadFile(url.toString());
                    /*dio=Dio();
                    courseContent.add(Course(title:"Chapter 2",path:"https://www.cs.purdue.edu/homes/ayg/CS251/slides/chap2.pdf"));*/
                    /*final taskId = await FlutterDownloader.enqueue(
                      url: url.toString(),
                      savedDir: (await getExternalStorageDirectory()).path,
                      showNotification: true,
                      // show download progress in status bar (for Android)
                      openFileFromNotification:
                          true, // click on notification to open downloaded file (for Android)
                    );*/
                  },
                  onLoadStop: (controller, url) async {
                    //pullToRefreshController.endRefreshing();
                    this.url = url.toString();
                    urlController.text = this.url;
                    /*setState(() {
                        this.url = url.toString();
                        urlController.text = this.url;
                      });*/
                  },
                  onLoadError: (controller, url, code, message) {
                    //pullToRefreshController.endRefreshing();
                  },
                  onProgressChanged: (controller, progress) {
                    model.setLoadProgress(progress);
                    if (progress == 100) {
                      //pullToRefreshController.endRefreshing();
                    }
                    this.url = url.toString();
                    urlController.text = this.url;
                    /*setState(() {
                        this.url = url.toString();
                        urlController.text = this.url;
                      });*/
                  },
                  onUpdateVisitedHistory: (controller, url, androidIsReload) {
                    this.url = url.toString();
                    urlController.text = this.url;
                    /*setState(() {
                        this.url = url.toString();
                        urlController.text = this.url;
                      });*/
                  },
                  onConsoleMessage: (controller, consoleMessage) {
                    print(consoleMessage);
                  },
                ),progress < 1.0
                ? LinearProgressIndicator(value: progress)
                : Container(),
            ],
          ),
        ),
            /////////////// OLD CODE
            /*Expanded(
              child: Container(
                child: InAppWebView(
                  initialOptions: InAppWebViewGroupOptions(
                      crossPlatform: InAppWebViewOptions(
                        debuggingEnabled: true,
                      ),
                      android: AndroidInAppWebViewOptions(
                          supportMultipleWindows: true)),
                  initialUrl:
                      'https://lendingportal.capital.trillioncove.com/JACCESS/m/',
                  onProgressChanged:
                      (InAppWebViewController controller, int progress) {
                    model.setLoadProgress(progress);
                  },
                  onWebViewCreated: (InAppWebViewController controller) {
                    _webViewController = controller;
                  },
                  onDownloadStart: (controller, url) async {
                    _navigationService.navigateTo(PdfRoute,
                        arguments: PdfArguments(
                          url: url,
                        ));

                  },
                  onCreateWindow: (controller, createWindowRequest) async {
                    print("onCreateWindow url creareWindowRequest.url");
                    String url = createWindowRequest.url;

                    if (url != null) {
                      if (url.contains('.pdf')) {
                        _navigationService.navigateTo(PdfRoute,
                            arguments: PdfArguments(
                              url: createWindowRequest.url,
                            ));
                      } else {
                        showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              content: Container(
                                width: MediaQuery.of(context).size.width,
                                height: 600,
                                child: InAppWebView(
                                  // Setting the windowId property is important here!
                                  windowId: createWindowRequest.windowId,
                                  initialOptions: InAppWebViewGroupOptions(
                                    crossPlatform: InAppWebViewOptions(
                                      debuggingEnabled: true,
                                    ),
                                  ),
                                  onWebViewCreated:
                                      (InAppWebViewController controller) {
                                    _webViewPopupController = controller;
                                  },
                                  onLoadStart:
                                      (InAppWebViewController controller,
                                          String url) {
                                    print("onLoadStart popup $url");
                                  },
                                  onLoadStop:
                                      (InAppWebViewController controller,
                                          String url) {
                                    print("onLoadStop popup $url");
                                  },
                                ),
                              ),
                            );
                          },
                        );
                      }

                    } else {}
                    return true;
                  },
                ),
              ),
            ),*/
          ],
        ),
      ),
    );
  }

*/
