import 'package:cove/constant/assets.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:stacked/stacked.dart';
import 'package:url_launcher/link.dart';

import 'custom_appbar.dart';

class ContactUsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: SystemUiOverlay.values);
    return Scaffold(
      body: _getBodyView(context),
    );
  }

  final Uri params = Uri(
    scheme: 'mailto',
    path: Assets.email,
    query: 'subject=App Feedback&body=Hello Sir', //add subject and body here
  );

  _getBodyView(BuildContext context) {
    return Center(
      child: Stack(
        alignment: AlignmentDirectional.bottomCenter,
        overflow: Overflow.visible,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                  Assets.mainBg,
                ),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Positioned(
            child: CustomAppBar(
              text: 'Contact Us',
              backgroundColor: Colors.grey[100],
              iconColor: Colors.purple,
              backIconBackgroundColor: Colors.white,
            ),
            top: 0,
            left: 0,
            right: 0,
          ),
          Positioned(
            top: 110,
            child: SizedBox(
              width: MediaQuery.of(context).size.width - 50,
              child: Card(
                elevation: 2,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Image(
                        image: AssetImage(Assets.phoneIcon),
                        width: 30,
                        height: 30,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Phone Number",
                        style: TextStyle(
                            color: Colors.purple[800],
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Quicksand-Bold',
                            fontSize: 14),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Link(
                          uri: Uri.parse("tel:+603 8602 9229"),
                          //target: LinkTarget.self,
                          builder: (context, followLink) {
                            return RichText(
                              text: TextSpan(children: [
                                TextSpan(
                                  text: "+603 8602 9229",
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.grey[800],
                                    fontWeight: FontWeight.normal,
                                    fontFamily: 'Quicksand-Bold',

                                  ),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = followLink,
                                ),
                              ]),
                            );
                          }),
                      /*Text(
                        "+603 8602 9229",
                        style: TextStyle(
                            color: Colors.grey[800],
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Quicksand-Bold',
                            fontSize: 14),
                        textAlign: TextAlign.center,
                      ),*/
                      SizedBox(
                        height: 20,
                      ),
                      Image(
                        image: AssetImage(Assets.emailIcon),
                        width: 30,
                        height: 30,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Email",
                        style: TextStyle(
                            color: Colors.purple[800],
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Quicksand-Bold',
                            fontSize: 14),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Link(
                          uri: params,
                          //target: LinkTarget.self,
                          builder: (context, followLink) {
                            return RichText(
                              text: TextSpan(children: [
                                TextSpan(
                                  text: Assets.email,
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.grey[800],
                                    fontWeight: FontWeight.normal,
                                    fontFamily: 'Quicksand-Bold',

                                  ),
                                  recognizer: TapGestureRecognizer()
                                  ..onTap = followLink,
                                ),
                              ]),
                            );
                          }),

                      /*Text(
                        Assets.email,
                        style: TextStyle(
                            color: Colors.grey[800],
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Quicksand-Bold',
                            fontSize: 14),
                        textAlign: TextAlign.center,
                      ),*/
                      SizedBox(
                        height: 20,
                      ),
                      Image(
                        image: AssetImage(Assets.webIcon),
                        width: 30,
                        height: 30,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Website",
                        style: TextStyle(
                            color: Colors.purple[800],
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Quicksand-Bold',
                            fontSize: 14),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Link(
                          uri: Uri.parse(Assets.contactUs),
                          //target: LinkTarget.self,
                          builder: (context, followLink) {
                            return RichText(
                              text: TextSpan(children: [
                                TextSpan(
                                  text: Assets.contactUs,
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.grey[800],
                                    fontWeight: FontWeight.normal,
                                    fontFamily: 'Quicksand-Bold',

                                  ),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = followLink,
                                ),
                              ]),
                            );
                          }),
                      /*Text(
                        Assets.contactUs,
                        style: TextStyle(
                            color: Colors.grey[800],
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Quicksand-Bold',
                            fontSize: 14),
                        textAlign: TextAlign.center,
                      ),*/
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
