import 'package:cove/constant/assets.dart';
import 'package:cove/routing/route_argument.dart';
import 'package:cove/routing/route_names.dart';
import 'package:cove/services/locator.dart';
import 'package:cove/services/navigator_service.dart';
import 'package:cove/view_model/webview_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:stacked/stacked.dart';
import 'package:url_launcher/url_launcher.dart';


import 'custom_appbar.dart';

class LearnMoreView extends StatelessWidget {
  final NavigationService _navigationService = locator<NavigationService>();

/*
  InAppWebViewController _webViewController;
  InAppWebViewController _webViewPopupController;
*/

  //////////// ROHIT
  final GlobalKey webViewKey = GlobalKey();

  InAppWebViewController webViewController;
  InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
      crossPlatform: InAppWebViewOptions(
        useShouldOverrideUrlLoading: true,
        mediaPlaybackRequiresUserGesture: false,
        allowUniversalAccessFromFileURLs: true,
        javaScriptCanOpenWindowsAutomatically: true,
        allowFileAccessFromFileURLs: true,
        useOnDownloadStart: true,
      ),
      android: AndroidInAppWebViewOptions(
        useHybridComposition: true,
        allowFileAccess: true,
      ),
      ios: IOSInAppWebViewOptions(
        allowsInlineMediaPlayback: true,
      ));

  //PullToRefreshController pullToRefreshController;
  String url = "";
  double progress = 0;
  final urlController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    //SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: SystemUiOverlay.values);

    return ViewModelBuilder<WebViewViewModel>.nonReactive(
      viewModelBuilder: () => WebViewViewModel(),
      builder: (_context, model, child) => Scaffold(
        /*floatingActionButton: SizedBox(
          height: 40,
          width: 40,
          child: FloatingActionButton(
            child: new Icon(
              Icons.arrow_back,
              color: Colors.purple,
            ),
            backgroundColor: Colors.white,
            focusColor: Colors.white,
            disabledElevation: 0.0,
            hoverColor: Colors.white,
            foregroundColor: Colors.white,
            hoverElevation: 0.0,
            shape: CircleBorder(side: BorderSide.none),
            onPressed: () {
              if (urlController.text.toString() == Assets.learnMore) {
                Navigator.pop(context);
              } else {
                webViewController?.goBack();
              }

              ///Navigator.pushReplacementNamed(context, "/logout");
            },
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
        backgroundColor: Colors.white,*/
        appBar:  AppBar (
          toolbarHeight: 1,
          backgroundColor: Colors.grey[200],
        ),
        body: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.grey[200],
              ),
              child: Padding(
                padding: EdgeInsets.only(top: 25),
                child: Row(
                  children: [
                    Expanded(
                      flex: 0,
                      child: GestureDetector(
                        onTap: () {
                          if (urlController.text.toString() == Assets.learnMore) {
                            Navigator.pop(context);
                          } else {
                            webViewController?.goBack();
                          }
                        },
                        child: Container(

                          margin: EdgeInsets.only(right: 20),
                          height: 40,
                          width: 65,
                          padding: EdgeInsets.all(10),
                          child: Center(
                            child: Image(
                              image: AssetImage(Assets.backArrow),
                              width: 25,
                              height: 25,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 5),
                        height: 40,
                        width: 65,
                        child: Text(
                          urlController.text.toString(),
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Quicksand-Bold',
                              color: Colors.purple),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            /*ButtonBar(
              alignment: MainAxisAlignment.values.first,
              children: <Widget>[
                ElevatedButton(
                  child: Icon(Icons.arrow_back),
                  onPressed: () {

                      if(urlController.text.toString()==Assets.learnMore){
                        Navigator.pop(context);
                      }else{
                        webViewController?.goBack();
                      }

                  },
                ),
              ],
            ),*/
            /* CustomAppBar(
              text: ' ',
              backgroundColor: Colors.grey[100],
              iconColor: Colors.purple,
              backIconBackgroundColor: Colors.white,
            ),
            Selector<WebViewViewModel, double>(
              selector: (context, model) => model.progress,
              builder: (context, progress, child) => Container(
                  margin: EdgeInsets.only(top: 5, right: 25, left: 25),
                  child: progress < 1.0
                      ? LinearProgressIndicator(
                          minHeight: 5,
                          backgroundColor: Colors.red,
                        )
                      : Container()),
            ),*/
            //////////// ROHIT
            //https://trillioncovecapital.com.my
            Expanded(
              child: Container(
                child: InAppWebView(
                  key: webViewKey,
                  initialUrlRequest: URLRequest(
                      url: Uri.parse(Assets.learnMore)),
                  initialOptions: options,
                  //pullToRefreshController: pullToRefreshController,
                  onWebViewCreated: (controller) {
                    webViewController = controller;
                  },
                  onLoadStart: (controller, url) {
                    this.url = url.toString();
                    urlController.text = this.url;
                    /*setState(() {
                        this.url = url.toString();
                        urlController.text = this.url;
                      });*/
                  },
                  androidOnPermissionRequest:
                      (controller, origin, resources) async {
                    return PermissionRequestResponse(
                        resources: resources,
                        action: PermissionRequestResponseAction.GRANT);
                  },
                  shouldOverrideUrlLoading:
                      (controller, navigationAction) async {
                    var uri = navigationAction.request.url;

                    if (![
                      "http",
                      "https",
                      "file",
                      "chrome",
                      "data",
                      "javascript",
                      "about"
                    ].contains(uri.scheme)) {
                      if (await canLaunch(url)) {
                        // Launch the App
                        await launch(
                          url,
                        );
                        // and cancel the request
                        return NavigationActionPolicy.CANCEL;
                      }
                    }

                    return NavigationActionPolicy.ALLOW;
                  },
                  onDownloadStart: (controller, url) async {
                    print("onDownloadStart $url");

                    _navigationService.navigateTo(PdfRoute,
                        arguments: PdfArguments(
                          url: url.toString(),
                        ));
                    /*final taskId = await FlutterDownloader.enqueue(
                      url: url.toString(),
                      savedDir: (await getExternalStorageDirectory()).path,
                      showNotification: true, // show download progress in status bar (for Android)
                      openFileFromNotification: true, // click on notification to open downloaded file (for Android)
                    );*/
                  },
                  onLoadStop: (controller, url) async {
                   // pullToRefreshController.endRefreshing();
                    this.url = url.toString();
                    urlController.text = this.url;
                    /*setState(() {
                        this.url = url.toString();
                        urlController.text = this.url;
                      });*/
                  },
                  onLoadError: (controller, url, code, message) {
                    //pullToRefreshController.endRefreshing();
                  },
                  onProgressChanged: (controller, progress) {
                    model.setLoadProgress(progress);
                    if (progress == 100) {
                      //pullToRefreshController.endRefreshing();
                    }
                    this.url = url.toString();
                    urlController.text = this.url;
                    /*setState(() {
                        this.url = url.toString();
                        urlController.text = this.url;
                      });*/
                  },
                  onUpdateVisitedHistory: (controller, url, androidIsReload) {
                    this.url = url.toString();
                    urlController.text = this.url;
                    /*setState(() {
                        this.url = url.toString();
                        urlController.text = this.url;
                      });*/
                  },
                  onConsoleMessage: (controller, consoleMessage) {
                    print(consoleMessage);
                  },
                ),
              ),
            ),
            /*Expanded(
              child: Container(
                child: InAppWebView(
                  initialOptions: InAppWebViewGroupOptions(
                      crossPlatform: InAppWebViewOptions(
                        debuggingEnabled: true,
                      ),
                      // set this to true if you are using window.open to open a new window with JavaScript
                      // javaScriptCanOpenWindowsAutomatically: true),
                      android: AndroidInAppWebViewOptions(
                          // on Android you need to set supportMultipleWindows to true,
                          // otherwise the onCreateWindow event won't be called
                          supportMultipleWindows: true)),
                  initialUrl: 'https://www.capital.trillioncove.com/',
                  onProgressChanged:
                      (InAppWebViewController controller, int progress) {
                    model.setLoadProgress(progress);
                  },
                  onWebViewCreated: (InAppWebViewController controller) {
                    _webViewController = controller;
                  },
                  onDownloadStart: (controller, url) async {
                    _navigationService.navigateTo(PdfRoute,
                        arguments: PdfArguments(
                          url: url,
                        ));


                  },
                  onCreateWindow: (controller, createWindowRequest) async {
                    print("onCreateWindow url creareWindowRequest.url");
                    String url = createWindowRequest.url;

                    if (url != null) {
                      if (url.contains('.pdf')) {
                        _navigationService.navigateTo(PdfRoute,
                            arguments: PdfArguments(
                              url: createWindowRequest.url,
                            ));
                      } else {
                        showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              content: Container(
                                width: MediaQuery.of(context).size.width,
                                height: 600,
                                child: InAppWebView(
                                  // Setting the windowId property is important here!
                                  windowId: createWindowRequest.windowId,
                                  initialOptions: InAppWebViewGroupOptions(
                                    crossPlatform: InAppWebViewOptions(
                                      debuggingEnabled: true,
                                    ),
                                  ),
                                  onWebViewCreated:
                                      (InAppWebViewController controller) {
                                    _webViewPopupController = controller;
                                  },
                                  onLoadStart:
                                      (InAppWebViewController controller,
                                          String url) {
                                    print("onLoadStart popup $url");
                                  },
                                  onLoadStop:
                                      (InAppWebViewController controller,
                                          String url) {
                                    print("onLoadStop popup $url");
                                  },
                                ),
                              ),
                            );
                          },
                        );
                      }

                    } else {}
                    return true;
                  },
                ),
              ),
            ),*/
          ],
        ),
      ),
    );
  }
}
