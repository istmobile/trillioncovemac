import 'package:cove/constant/app_colors.dart';
import 'package:cove/constant/assets.dart';
import 'package:cove/customView/circle_painter.dart';
import 'package:cove/customView/line_painter.dart';
import 'package:cove/routing/route_names.dart';
import 'package:cove/services/locator.dart';
import 'package:cove/services/navigator_service.dart';
import 'package:cove/versionCheck/version_check.dart';
import 'package:cove/view/page_one.dart';
import 'package:cove/view/page_three.dart';
import 'package:cove/view/page_two.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:stacked/stacked.dart';



class PageOneTempView extends StatefulWidget {
  NewView createState() => NewView();

}

class NewView extends State<PageOneTempView> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    //SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);

    //versionCheck(context);

    @override
    void initState() {
      super.initState();
    }

    return Scaffold(
      key: _scaffoldKey,
      body: _getBodyView(context),
    );
  }

  _getBodyView(BuildContext context) {
    return Center(
      child: Stack(
        alignment: AlignmentDirectional.bottomCenter,
        overflow: Overflow.visible,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                  Assets.mainBg,
                ),
                fit: BoxFit.cover,
              ),
            ),
          ),
          PageView(
            controller: Assets.page_Controller,
            children: <Widget>[
              PageOneView(),
              PageTwoView(),
              PageThreeView(),
            ],
          ),
        ],
      ),
    );
  }
}
