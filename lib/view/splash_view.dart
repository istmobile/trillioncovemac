import 'package:cove/constant/assets.dart';
import 'package:cove/routing/route_names.dart';
import 'package:cove/services/locator.dart';
import 'package:cove/services/navigator_service.dart';
import 'package:cove/view/page_one_temp.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:stacked/stacked.dart';

class SplashView extends StatelessWidget {
  final NavigationService _navigationService = locator<NavigationService>();
  @override
  Widget build(BuildContext context) {
    //SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: SystemUiOverlay.values);
    //SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);

    _nextPage();
    return Scaffold(
      body: _getBodyView(context),
    );
  }

  _nextPage() {
    Future.delayed(Duration(seconds: 5), () async {
      _navigationService.navigateToBack(
        PageOneTempRoute,
      );
    });
  }

  _getBodyView(BuildContext context) {
    return Center(
      child: Stack(
        alignment: AlignmentDirectional.bottomCenter,
        overflow: Overflow.visible,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                  Assets.mainBg,
                ),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Center(
            child: Container(
              margin: EdgeInsets.only(bottom: 10),
              child: Image(
                image: AssetImage(Assets.mainLogo),
                width: 250,
                height: 100,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
