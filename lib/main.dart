import 'dart:isolate';
import 'dart:ui';

import 'package:cove/routing/app_router.dart';
import 'package:cove/services/locator.dart';
import 'package:cove/services/navigator_service.dart';
import 'package:cove/versionCheck/version_check.dart';
import 'package:cove/view/splash_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'constant/app_colors.dart';

import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:path_provider/path_provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:io';

const debug = true;
PullToRefreshController pullToRefreshController;
InAppWebViewController webViewController;
double progress = 0;


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  /*if (Platform.isAndroid) {
    await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
  }*/
/*  await FlutterDownloader.initialize(
      debug: true // optional: set false to disable printing logs to console
      );*/
  await Permission.camera.request();
  await Permission.microphone.request();

  //await Permission.storage.request();
  //FlutterDownloader.registerCallback(downloadCallback);
  setupLocator();
  runApp(MyHomePage());
}
class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends  State<MyHomePage> {
  // This widget is the root of your application.

  @override
  void initState() {
    super.initState();
    showStatusBar();
  }


  @override
  Widget build(BuildContext context) {
   // showStatusBar();
   // Firebase.initializeApp();

    /*SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.dark.copyWith(

          // statusBarColor is used to set Status bar color in Android devices.
          statusBarColor: Color(0xFFFFFFFF),

          // To make Status bar icons color white in Android devices.
          statusBarIconBrightness: Brightness.dark,

          // statusBarBrightness is used to set Status bar icon color in iOS.
          statusBarBrightness:
              Brightness.light // Here light means dark color Status bar icons.

          ),
    );*/

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        appBarTheme: Theme.of(context).appBarTheme.copyWith(
              brightness: Brightness.light,
              color: Colors.white,
              elevation: 0,
            ),
        accentColor: primaryColor,
        highlightColor: Colors.transparent,
        splashColor: Colors.transparent,
        fontFamily: 'SFUIDisplay-Regular',
        scaffoldBackgroundColor: Colors.white,
      ),
      navigatorKey: locator<NavigationService>().navigatorKey,
      onGenerateRoute: AppRouter.generateRoute,
      home: SplashView(),
      builder: (BuildContext context, Widget child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
          child: child,
        );
      },
    );
  }
  Future showStatusBar() =>
      SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: SystemUiOverlay.values);
}
